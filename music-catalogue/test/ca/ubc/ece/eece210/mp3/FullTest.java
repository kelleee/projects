package ca.ubc.ece.eece210.mp3;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

public class FullTest {

	@Test
	public void test() {
		// @Test 1 : Add an album to genre
		Genre genre = new Genre ("Rock");
		ArrayList<String> songlist = new ArrayList <String> ();
		songlist.add("Hey Jude");
		songlist.add("Yesterday");
		Album album = new Album ("The White Album", "The Beatles", songlist);
		
		album.addToGenre(genre);
		
		assertEquals("Rock", album.getGenre().getName());
		assertEquals(true, genre.hasChildren());
		
		// adds another album created from a string representation
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		Album album2 = new Album (stringRep);
		
		Genre genre2 = new Genre ("Pop");
		album2.addToGenre(genre2);
		
		assertEquals("Pop", album2.getGenre().getName());
		assertEquals(true, genre2.hasChildren());
		
		// adds a second album to the same genre
		album2.addToGenre(genre);
		assertEquals(true, genre.hasChildren());
		assertEquals(2, genre.getChildren().size());
		
	}
	
	@Test
	public void test2() {
		// @Test 2 : Remove an album from genre
		Genre genre = new Genre ("Rock");
		
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		String stringRep2 = "\nRush\nEric Clapton\n[Tears in Heaven, Help Me Up]";
		Album album = new Album (stringRep);
		Album album2 = new Album (stringRep2);
		
		genre.addToGenre(album);
		genre.addToGenre(album2);
		
		assertEquals(2,genre.getChildren().size());
		
		// removes an album
		genre.removeFromGenre(album2);
		assertEquals(1,genre.getChildren().size());
		
		// removes another album
		genre.removeFromGenre(album);
		assertEquals(0,genre.getChildren().size());
		
		// attempt to remove the same album, will throw NullPointerException because such element does not exist anymore
		try {
			genre.removeFromGenre(album);
			fail();
		} catch (NullPointerException e) {
			assertEquals(e.getMessage(), "No element is found");
		}
	}
	
	@Test
	public void test3() {
		// @Test 3 : Save an album in the string form
		ArrayList<String> songlist = new ArrayList <String> ();
		songlist.add("Hey Jude");
		songlist.add("Yesterday");
		Album album = new Album ("The White Album", "The Beatles", songlist);
		
		// gets the title, performer & songs from the created album
		assertEquals("The White Album",album.getTitle());
		assertEquals("The Beatles",album.getPerformer());
		assertEquals("Hey Jude", album.getSongs().get(0));
		assertEquals("Yesterday", album.getSongs().get(1));
		
		// gets a string representation of the created album
		assertEquals("The White Album\nThe Beatles\n[Hey Jude, Yesterday]\n",album.getStringRepresentation());
	}
	
	@Test
	public void test4() {
		// @Test 4 : Recreate album from a string representation
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		Album album = new Album (stringRep);
		
		// gets the title, performer & songs from the created album
		assertEquals("Love Is A Four Letter Word",album.getTitle());
		assertEquals("Jason Mraz",album.getPerformer());
		assertEquals("I won't give up", album.getSongs().get(0));
		assertEquals("Distance", album.getSongs().get(1)); // check white space -.-
		
	}
	
	@Test
	public void test5() {
		// @Test 5 : Save a genre to the string form
		Genre genre = new Genre ("Jazz");
		assertEquals ("Jazz\n", genre.getStringRepresentation());
		
	}
	
	@Test
	public void test6() {
		// @Test 6 : Recreate a Genre from a string form
		String stringRepresentation = "Jazz";
		Genre genre = Genre.restoreCollection(stringRepresentation);
		assertEquals("Jazz", genre.getName());

		// is Genre restored from a string representation is an instance of Genre?
		assertEquals(true, genre instanceof Genre);
	}

	@Test
	public void test7() throws IOException {
		// @Test 7 : Save the whole catalogue to a file
		
		Catalogue MusicCatalogue = new Catalogue();
		
		//create elements
		String genreRep = "Other";
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		String stringRep2 = "\nRush\nEric Clapton\n[Tears in Heaven, Help Me Up]";
		
		Genre genre = new Genre (genreRep);
		Album album = new Album (stringRep);
		Album album2 = new Album (stringRep2);
		
		genre.addToGenre(album);
		genre.addToGenre(album2);
		
		MusicCatalogue.addElement(genre);;
		MusicCatalogue.addElement( album );
		MusicCatalogue.addElement( album2 );
		
		//Test includes albums and top genres only
		MusicCatalogue.saveCatalogueToFile("test.txt");
		
		assertEquals( 3, MusicCatalogue.catalogueSize() );
		//check file for correct data
		
		//Test includes albums only
		MusicCatalogue.removeElement(genre);
		MusicCatalogue.saveCatalogueToFile("test2.txt");
		
		assertEquals (2, MusicCatalogue.catalogueSize());
		//check file for correct data
		
		//Test includes albums, top genres, subgenres
		MusicCatalogue.addElement(genre);
		Genre topGenre = new Genre ("topGenre");
		topGenre.addToGenre(genre);
		MusicCatalogue.addElement(topGenre);
		MusicCatalogue.saveCatalogueToFile("test3.txt");
		
		assertEquals (4, MusicCatalogue.catalogueSize());
		//check file for correct data
	}
	
	@Test
	public void test8() throws IOException {
		// @Test 8 : Recreate the catalogue from a file
		
		//test file includes only albums
		Catalogue MusicCatalogue = new Catalogue ("test4.txt");
		MusicCatalogue.saveCatalogueToFile("test5.txt");	//try rewriting to test if worked
		assertEquals(3, MusicCatalogue.catalogueSize());
		
		//test file includes albums and top genre
		Catalogue MusicCatalogue2 = new Catalogue ("test6.txt");
		MusicCatalogue.saveCatalogueToFile("test7.txt");	//try rewriting to test if worked
		assertEquals(5, MusicCatalogue2.catalogueSize());
		
		//test file includes albums, top genres, and subgenres
		Catalogue MusicCatalogue3 = new Catalogue ("test8.txt");
		MusicCatalogue.saveCatalogueToFile("test9.txt");	//try rewriting to test if worked
		assertEquals(6, MusicCatalogue3.catalogueSize());
	}
	
	@Test
	public void test9() {
		// @Test 9 : Test to verify Genre inclusion rule
		Genre genre = new Genre ("Rock");
		Genre subgenre = new Genre ("Alternative Rock");
		String stringRep = "\nRush\nEric Clapton\n[Tears in Heaven, Help Me Up]";
		Album album = new Album (stringRep);
		
		// adds an album and a subgenre to main genre
		genre.addToGenre(album);
		genre.addToGenre(subgenre);
		assertEquals(2, genre.getChildren().size());
		
		// attempt to add album to the subgenre, will throw IllegalArgumentException 
		// because the album is already part of the main genre
		try {
			subgenre.addToGenre(album);
	
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "This album is contained in the main genre");
		}
		
		
		
	}

}