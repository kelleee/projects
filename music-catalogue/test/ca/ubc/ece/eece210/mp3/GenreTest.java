package ca.ubc.ece.eece210.mp3;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class GenreTest {

	@Test
	public void test() {
		// creates genre with the given name
		Genre test = new Genre("Rock");

		assertEquals("Rock", test.getName());
		assertEquals("Rock", test.getStringRepresentation());

	}

	@Test
	public void test2() {
		// adding album to the genre
		Genre thisGenre = new Genre("Rock");
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Hey Jude");
		songlist.add("Yesterday");

		Album newAlbum = new Album("The White Album", "The Beatles", songlist);
		
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		Album newAlbum2 = new Album (stringRep);
		
		newAlbum.addToGenre(thisGenre);
		newAlbum2.addToGenre(thisGenre);
	
		assertEquals("Rock", newAlbum.getGenre().getName());
		assertEquals("Rock", newAlbum2.getGenre().getName());
		
		assertEquals(true, thisGenre.hasChildren());

		assertEquals(2, thisGenre.getChildren().size());

	}

}
