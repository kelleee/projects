package ca.ubc.ece.eece210.mp3;

import static org.junit.Assert.*;
import java.util.ArrayList;


import org.junit.Test;

public class AlbumTest {

	

	@Test
	public void test() {
		//Builds an album with the given title, performer and song lists
		ArrayList<String> songlist = new ArrayList <String> ();
		songlist.add("Hey Jude");
		songlist.add("Yesterday");
			
		Album test = new Album ("The White Album", "The Beatles", songlist);
		
		assertEquals("The White Album",test.getTitle());
		assertEquals("The Beatles",test.getPerformer());
		assertEquals("Hey Jude", test.getSongs().get(0));
		assertEquals("Yesterday", test.getSongs().get(1));
		
		System.out.println(test.getStringRepresentation());
	}
	
	@Test
	public void test2() {
		//Builds an album from a string representation
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		Album test2 = new Album (stringRep);
		
		assertEquals("Love Is A Four Letter Word",test2.getTitle());
		assertEquals("Jason Mraz",test2.getPerformer());
		assertEquals("I won't give up", test2.getSongs().get(0));
		
		System.out.println(test2.getStringRepresentation());
	}



}
