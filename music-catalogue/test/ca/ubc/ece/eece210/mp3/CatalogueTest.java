package ca.ubc.ece.eece210.mp3;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

public class CatalogueTest {
	
	@Test
	public void test() throws FileNotFoundException, IOException {
		// @test1 : Save Catalogue to a file
		
		
		String stringRep = "\nLove Is A Four Letter Word\nJason Mraz\n[I won't give up, Distance]";
		Album album1 = new Album (stringRep);
		
		Genre genre = new Genre ("Rock");
		Genre subGenre = new Genre ("Alternative Rock");
		genre.addToGenre(album1);
		
		ArrayList<String> songlist = new ArrayList <String> ();
		songlist.add("Hey Jude");
		songlist.add("Yesterday");
		Album album2 = new Album ("The White Album", "The Beatles", songlist);
		
		subGenre.addToGenre(album2);
		genre.addToGenre(subGenre);
		
		String stringrep2 = "\nHome\nMichael Buble\n[song 1, song 2, song3]";
		Album album3 = new Album (stringrep2);
		
		String stringRep3 = "\nLego House\nEd Sheraan\n[song1, song2, song3, song4]";
		Album album4 = new Album (stringRep3);
		Genre subGenre2 = new Genre ("Soft Rock");
		subGenre2.addToGenre(album4);
		subGenre.addToGenre(subGenre2);
		
		
		Genre genre2 = new Genre ("Jazz");
		genre2.addToGenre(album3);
		
		Catalogue test = new Catalogue();
		test.addElement(genre);
		test.addElement(genre2);
		
		test.saveCatalogueToFile("mytest.txt");
		//System.out.println(test2);
	}

}
