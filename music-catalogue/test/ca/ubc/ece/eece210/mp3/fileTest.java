package ca.ubc.ece.eece210.mp3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;


/*
 * I am using this class to figure out writing to files works for catalogue
 */
public class fileTest {
	
	@Test
	public void fileTest() {
		try {
	ArrayList <String> myArray = new ArrayList <String>();
	
	
		String sCurrentLine;
		
		BufferedReader br = new BufferedReader (new FileReader ("testFile.txt"));
		
		while ((sCurrentLine = br.readLine () ) != null ) {
			myArray.add(sCurrentLine);
		}
		
		for (int i = 0; i < myArray.size(); i++) {
			System.out.println(myArray.get(i));
		}
		
		br.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}
}
