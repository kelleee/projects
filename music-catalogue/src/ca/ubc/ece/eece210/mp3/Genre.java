package ca.ubc.ece.eece210.mp3;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Theresa Mammarella, Stephanie Lam, Kelly Anyi
 * 
 */
public final class Genre extends Element {
	
	private String name;
	private Genre genre;
	
	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name- the name of the genre.
	 */
	public Genre(String name) {
			this.setName(name);		
	}

	
	/**
	 * Restores a genre from its given string representation.
	 * 
	 * @param stringRepresentation
	 * @return genre 
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		Genre genre = new Genre(stringRepresentation);
		return genre;
	}

	/**
	 * Returns the string representation of a genre
	 * 
	 * @return stringRep- string representation of genre
	 */
	public String getStringRepresentation() {
		String stringRep = name + "\n";
		return stringRep;
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @param b - the element to be added to the collection.
	 * @throws IllegalArgumentException- if album is already contained in an album
	 */
	public void addToGenre(Element b) {
		if (b instanceof Album){
			if (b instanceof Genre)
			{
				throw new IllegalArgumentException ("This album is contained in the main genre");
			}
			else
				this.addChild(b);		
		}
		else
			this.addChild(b);
	}
	
	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 * 
	 * @return boolean- true when called
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	/**
	 * Removes the given album or genre from this genre
	 * 
	 * @param b - the element to be removed from the collection
	 */
	public void removeFromGenre(Element b) {
			this.removeChild(b);
	}

	/**
	 * Returns the name of the genre
	 * 
	 * @return name- genre name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Mutator for Genre
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the genre
	 * 
	 * @return the album genre
	 */
	public Genre getGenre() {
		return genre;
	}
	
	/**
	 * Set the album genre
	 * 
	 * @param genre
	 */
	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	
	/**
	 * Override equals for genres
	 * MP4 method
	 */
	@Override
	public boolean equals (Object object) {
	//Genres are equal with the same name
		if (object == null) {
			return false;
		}
		Genre myObj = (Genre) object;
		
		if ( this.name.equals(myObj.getName()) ){
			return true;
		} else {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

}