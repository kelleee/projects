package ca.ubc.ece.eece210.mp3;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * 
 * @author Kelly Anyi, Theresa Mammarella, Stephanie Lam 
 * 
 * This class contains the information needed to represent 
 * an album in our application.
 * 
 */
public class Album extends Element {
	private String title;
	private String performer;
	private ArrayList<String> songlist = new ArrayList<String> ();
	private Genre genre;
	
	/**
	 * Builds a book with the given title, performer and song list
	 * 
	 * @param title- the title of the album
	 * @param author- the performer 
	 * @param songlist- the list of songs in the album
	 */
	public Album (String title, String performer, ArrayList<String> songlist) {
		this.title = title;
		this.performer = performer;
		this.songlist = songlist;
	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * 
	 * @param stringRepresentation- the string representation
	 */
	public Album (String stringRepresentation) {
		StringTokenizer stringTokenizer = new StringTokenizer(stringRepresentation, "\n");
		
		while (stringTokenizer.hasMoreElements()) {
			
			this.title = stringTokenizer.nextToken();
		    this.performer = stringTokenizer.nextToken();
		    String list = stringTokenizer.nextToken();
		   	String sublist = list.substring(1, list.length()-1); // remove '[' and ']'
		   	StringTokenizer songTokenizer = new StringTokenizer(sublist, ",");
		   	while (songTokenizer.hasMoreElements())
		   	{
		   		String song = songTokenizer.nextToken().trim(); // removes leading/trailing whitespaces
		   		this.songlist.add(song);
		   	}
		}
	}

	/**
	 * Returns the string representation of the given album. The representation
	 * contains the title, performer and songlist, as well as all the genre
	 * that the book belongs to.
	 * 
	 * @return the string representation
	 */
	public String getStringRepresentation() {
		String stringRepresentation;
		stringRepresentation = title + "\n" + performer + "\n" + songlist + "\n";
		return stringRepresentation;
	}
	
	/**
	 * Add the book to the given genre
	 * 
	 * @param genre- the genre to add the album to.
	 */
	public void addToGenre(Genre genre) {
		genre.addChild(this);
		setGenre(genre);
	}
	
	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to
	 */
	public Genre getGenre() {
		return genre;
	}
	
	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Returns the performer of the album
	 * 
	 * @return performer
	 */
	public String getPerformer() {
		return performer;
	}
	
	/**
	 * An album cannot have any children (it cannot contain anything).
	 * 
	 * @return boolean- false when called
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
	
	/**
	 * Sets the genre that the album belongs to
	 * 
	 * @param genre
	 */
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	/**
	 * Returns the list of songs
	 * 
	 * @return songList
	 */
	public ArrayList<String> getSongs() {
		return songlist;
	}
	
	/**
	 * Override equals for albums
	 * MP4 method
	 */
	@Override
	public boolean equals (Object object) {
	//Albums are equal with the same performer and title
		if (object == null) {
			return false;
		}
		Album myObj = (Album) object;
		
		if ( this.title.equals(myObj.getTitle()) && this.performer.equals(myObj.getPerformer())){
			return true;
		} else {
			return false;
		}
	}
	
    @Override
    public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
              + ((performer == null) ? 0 : performer.hashCode());
    result = prime * result + ((title == null) ? 0 : title.hashCode());
    return result;
  }
}