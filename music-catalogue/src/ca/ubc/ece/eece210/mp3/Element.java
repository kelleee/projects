package ca.ubc.ece.eece210.mp3;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class to represent an entity in the catalogue. The element (in this
 * implementation) can either be an album or a genre.
 * 
 * @author Stephanie Lam, Kelly Anyi, Theresa Mammarella
 * 
 */
public abstract class Element {

	private List<Element> children = new ArrayList<Element>();

	/**
	 * Returns all the children of this entity. They can be albums or
	 * genres. In this particular application, only genres can have
	 * children. Therefore, this method will return the albums or genres
	 * contained in this genre.
	 * 
	 * @return children of album
	 */
	public List<Element> getChildren() {
		if(hasChildren() == true) {
			return this.children;
		}
		else {
			System.out.println("This entity does not have children.");
			return new ArrayList<Element>();
		}
	}
	
	/**
	 * Adds a child to this entity. Basically, it is adding an album or genre
	 * to an existing genre
	 * 
	 * @param b- the element to be added to the existing genre
	 * @throws IllegalArgumentException- if genre already contains element
	 * @throws IllegalArgumentException- if user tries to add an element to an album
	 */
	protected void addChild(Element b) {
		
		if(hasChildren() == true) {
		
			if ( getChildren().contains(b) ) {
				throw new IllegalArgumentException("This element is already a child to the entity.");	
			}
			else
			{
				getChildren().add(b);
			}
		}
		else {
			throw new IllegalArgumentException ("Children cannot be added to album type.");
		}
	}
	 
	/**
	 * Abstract method to determine if a given entity can (or cannot) contain
	 * any children.
	 * 
	 * @return true if the entity can contain children, or false otherwise.
	 */
	public abstract boolean hasChildren();
	
	/**
	 * Returns true if Genre genre is a subgenre, else returns false
	 * 
	 * @param musicCatalogue
	 * @param genre
	 * @return
	 */
	 public Element getParent (  ArrayList<Element> musicCatalogue) {
		ArrayList <Element> genreChildren = new ArrayList <Element> ();
		Element parent = null; 
		
		 for ( int i = 0; i < musicCatalogue.size(); i++ ) {
			
			 if ( musicCatalogue.get(i).hasChildren() ) {	//genre
				 genreChildren.addAll( musicCatalogue.get(i).getChildren() );
			
				 if (genreChildren.contains(this)) {
					 parent =  musicCatalogue.get(i);
					 break;
				 }
				 
				 genreChildren.clear();
			 }
		 }
		 return parent;
	 }
	 
	/**
	 * Removes the specified child from this entity.
	 * 
	 * @param b- element to be removed
	 * @throws NullPointerException- if entity contains no children
	 * @throws NullPointerException- if b is not a child of the entity
	 */
	protected void removeChild(Element b){
		if (hasChildren() == true){
			if ( children.contains(b) )
				getChildren().remove(b);
			else
				throw new NullPointerException ("No element is found");	
		}
		else
			throw new NullPointerException ("This entity has no children");	
	}
	
	/**
	 * Sets children of an entity.
	 * 
	 * @param children
	 */
	public void setChildren(List<Element> children) {
		this.children = children;
	}

	
	
	/**
	 * Abstract method for getting string representation
	 */
	public abstract String getStringRepresentation();
	
	/**
	 * hashcode and equals override abstract
	 */
	public abstract int hashCode();
	public abstract boolean equals (Object object);
}