package ca.ubc.ece.eece210.mp3;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * 
 * @author Theresa Mammarella, Stephanie Lam, Kelly Anyi
 * 
 */
public class Catalogue {
	protected List<Element> element;
	protected ArrayList <Element> MusicCatalogue;
	private Album album;
	private String title;
	private String performer;
	protected ArrayList <String> songlist;
	
	private Genre genre;
	private Genre subGenre;
	
	private String fileName;
	private File file;
	private FileWriter myFile;
	private BufferedWriter CatalogueFile;
	/**
	 * Builds a new, empty catalogue.
	 * @param MusicCatalogue
	 */
	public Catalogue() {
		this.MusicCatalogue = new ArrayList<Element> ();
	}
	
	/**
	 * Returns the size of the catalogue
	 */
	public int catalogueSize () {
		return MusicCatalogue.size();
	}
	
	/**
	 * Contains method for catalogue
	 */
	public boolean catContains (Element e) {
		return MusicCatalogue.contains(e);
	}
	/**
	 * Adds element to catalogue
	 * @param e
	 */
	public void addElement ( Element e ) {
		MusicCatalogue.add(e);
	}
	
	/**
	 * Removes element from catalogue
	 * @param e
	 */
	public void removeElement (Element e) {
		MusicCatalogue.remove(e);
	}
	
	/**
	 * gets element from catalogue
	 * @param i
	 */
	public Element getElement (int i) {
		return MusicCatalogue.get(i);
	}
	 
	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName- the file from where to restore the library.
	 * @throws FileNotFoundException 
	 */
	 public Catalogue(String fileName) throws FileNotFoundException, IOException {
		 	this.MusicCatalogue = new ArrayList<Element> ();
			
			//This part of the code partitions the file data into genres and albums
			//Each element is then added to the music catalogue
		 	BufferedReader ElementString = new BufferedReader (new FileReader (fileName));
			String myLine;
			
			while ((myLine = ElementString.readLine()) != null){
				String nextLine = ElementString.readLine();
				if (nextLine.equals("")) {	//this is a genre
					//This code works to create a genre, regardless of indents
					title = myLine.trim();
					this.genre = new Genre (title);
					MusicCatalogue.add(genre);
				} else {	//this is an album
					//This code works to create an album, regardless of indents
					title = myLine.trim();
					performer = nextLine.trim();
					String songs = ElementString.readLine().trim();
					songs = songs.substring(1, songs.length()-1);	//removes [ and ]
					ArrayList<String> mySongList = new ArrayList <String> ();
					for (String song: songs.split(",")) {
						mySongList.add(song);
					}
					this.album = new Album( title, performer, mySongList);
					MusicCatalogue.add(album);
					myLine = ElementString.readLine();//skips space after album
				}
			}
			ElementString.close();
			
			//TODO test code
			//This part of the code implements the genre/ album hierarchy
			//Code rescans the file, this time looking for indents per element
			BufferedReader HierString = new BufferedReader (new FileReader (fileName));

			String tabLine;
			String trimTabLine;
			int indent;
			int lastIndent = 0;
			
			for ( int i = 0; i < MusicCatalogue.size(); i++ ) {
				tabLine = HierString.readLine();
				
				//equates line to its trimmed version in order to
				//determine the number of indents
				trimTabLine = tabLine.trim();
				indent = 0;
				while ( ! tabLine.equals(trimTabLine) ) {
					tabLine.substring(1, tabLine.length()-1);
					indent++;
				}
				
				//assigns parent to current element depending on indents
				if ( indent > lastIndent ) {	//element is a child of parent
					MusicCatalogue.get(i-1).addChild(MusicCatalogue.get(i));
				} else if ( indent == lastIndent ){	//elements are siblings
					MusicCatalogue.get(i-1).getParent(MusicCatalogue).addChild(MusicCatalogue.get(i));
				} else {	//element is an aunt of last element
					MusicCatalogue.get(i-1).getParent(MusicCatalogue).getParent(MusicCatalogue).addChild(MusicCatalogue.get(i));
				}
				
				//Skips through file lines according to element type
				if (MusicCatalogue.get(i) instanceof Genre ){
					tabLine = HierString.readLine();
				} else {	//element is an album
					tabLine = HierString.readLine();
					tabLine = HierString.readLine();
					tabLine = HierString.readLine();
				}	
			}
			HierString.close();
			
			System.out.println("Done reading");
	 }
	 
	/**
	 * Saved the contents of the catalogue to the given file.
	 * 
	 * @param fileName the file where to save the library
	 * @throws FileNotFoundException
	 * @throws IOException 
	 */
	 public void saveCatalogueToFile(String fileName) throws FileNotFoundException, IOException {
			
			// Cannot overwrite existing file
			if (MusicCatalogue.isEmpty() ) {
				throw new IllegalArgumentException ( "Error: not an empty catalogue." );
			}
			
			// Initializes file writer
			this.fileName = fileName;
			this.file = new File(fileName);
			this.myFile = new FileWriter (file.getAbsoluteFile());
			this.CatalogueFile = new BufferedWriter ( myFile );
			
			String elemStr;
			String indent = "\t";
			for ( int i = 0; i < MusicCatalogue.size(); i++ ) {
				
				if ( MusicCatalogue.get(i).getParent(MusicCatalogue) == null ) {	//if i is a top element
					elemStr = MusicCatalogue.get(i).getStringRepresentation();
				
					if (MusicCatalogue.get(i) instanceof Album ) {	//top element is an album
						CatalogueFile.write (elemStr + "\n");
					} else											//top element is a genre
						CatalogueFile.write (elemStr + "\n");
						element = MusicCatalogue.get(i).getChildren();
				
						for (int c = 0; c < element.size(); c++){	//prints top genre's children
						
							if (element.get(c).hasChildren() == false) {	//element is an album
								
								String str1;
								str1 = element.get(c).getStringRepresentation();
								str1 = str1.replaceAll("\n", "\n" + indent);
								printAlbum(str1);
							}
							
							if (element.get(c).hasChildren() == true) {	//element is a subgenre
								String str2;
								str2 = element.get(c).getStringRepresentation();
								printGenre(str2);
								
								element = element.get(c).getChildren();
								
								for (int j = 0; j < element.size(); j++) {
									if (element.get(j).hasChildren() == true) {
										String str3;
										str3 = element.get(j).getStringRepresentation();
										indent = indent.concat("\t");
										str3 = str3.replaceFirst("", "\n" + indent);
										printGenre(str3);
										
										element = element.get(j).getChildren();
										j = 0;
										
									} else {
										String str4;
										str4 = element.get(j).getStringRepresentation();
										indent = indent.concat("\t");
										str4 = str4.replaceAll("\n", "\n" + indent);
										printAlbum(str4);
										
									}
								}
										
							}

						}

				}
			}
			CatalogueFile.close();
			System.out.println("Done"); 
		}

		public void printAlbum (String str1) throws IOException {
			System.out.println("\t" + str1);
			CatalogueFile.write("\t" + str1 + "\n");
		
		}
		public void printGenre (String str2) throws IOException{
			System.out.println("\t" + str2);
			CatalogueFile.write("\t" + str2 + "\n");
		
		}
	
		
/* MP4 methods*/
		
		public List<Album> queryCat( String query) {
			return null;	//TODO
		}
		
		/*
		 * New override equals method for mp4
		 * 
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals (Object object) {
			
			if (object == null) {
				return false;
			}
			
			Catalogue objCat = (Catalogue) object;
			
			if (this.MusicCatalogue.size() != objCat.MusicCatalogue.size()) {
				return false;
			}
			
			for (int i = 0; i < this.MusicCatalogue.size(); i++) {
				if ( this.MusicCatalogue.get(i).equals(objCat.MusicCatalogue.get(i))) {
					continue;
				} else {
					return false;
				}
			}
			return true;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
		    int result = 1;
		    result = prime * result
		                + ((MusicCatalogue == null) ? 0 : MusicCatalogue.hashCode());
		    return result;
		 }
	}