/*	File	: Deck.hpp
 *	Modified: October 28, 2013
 *
 * ----- This file declares the Deck class
 */

#ifndef _DECK_HPP_
#define _DECK_HPP_

#include "Card.hpp"

class Deck {
	public:
		// Constructor
		Deck();
	
		// Function to shuffle the cards
		void shuffle();

		// Function to display the deck of cards
		void displayCards();
	
		// Function to draw a card from the deck
		Card getCard();

	private:
		Card hand[52];
		Card draw_;
		Card dealer_;
	
};

#endif
