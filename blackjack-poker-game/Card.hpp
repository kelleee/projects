/*	File	: Deck.hpp
 *	Modified: October 28, 2013
 *
 * ----- This file declares the Card class
 */

#ifndef _CARD_HPP_
#define _CARD_HPP_

#include <string>
#include <ctime>	// This library randomize everything according to current time
#include <iomanip>	// Formatting

using namespace std;

class Card {
	public:
		// Constructor
		Card(int = 0, char = ' ');

		// Function to set the card to a specific value
		void setCard (int, char);
		
		// Function to display the cards
		void display();
		void displayHole();
		
		// Function to swap the cards -- give random cards shuffling
		void swap(Card&);			

		// Accessors / Getters
		int getRank ();
		char getSuit();

	private:
		char suit_;
		int rank_;
};

#endif