/*	File	: Blackjack.cpp
 *	Modified: October 28, 2013
 *
 * ----- This file defines the Blackjack class
 */

#include "Blackjack.hpp"
#include "Card.hpp"
#include "Deck.hpp"
#include <iostream>
#include <string>

const int MAX = 21;	// the maximum card values each hand can hold

Deck playingDeck;	// creates a deck of card to be play in the game

Blackjack::Blackjack()
{
	this->numPlayers_ = 0;
	this->gameLimit_ = 0;
}

//--- This constructor creates a game based on the number of players and game limit
Blackjack::Blackjack( int numPlayers, int gameLimit )
{
	this->numPlayers_ = numPlayers;
	this->gameLimit_ = gameLimit;
	setPlayerBank (numPlayers_);
	setPlayer (numPlayers_);
	setCardValues (numPlayers_);
	this->gameRound_ = 1;
}

//--- This function asks for user input and create a new game based on the inputs
void Blackjack::newGame()
{
	int numPlayers;
	int limit;
	
	cout << endl << "How many players are playing?: ";
	
	while (!(cin >> numPlayers) || ( numPlayers > 10))
	{
		cout << endl << "< Note > Maximum number of players is 10 :\nHow many players are playing? "  ;
		cin.clear();
		cin.ignore(100, '\n');
	}

	cout << "How many turns do you want to play? ";
	
	while (!(cin >> limit) || ( limit > 10))
	{
		cout << endl << " < Note > You can play only up to 10 turns :\nHow many turns do you want to play? ";
		cin.clear();
		cin.ignore(100, '\n');
	}

	Blackjack newGame(numPlayers,limit);
	newGame.displayInitial();
	newGame.playRound();
}

//--- This function initialize player's bank to $1000
void Blackjack::setPlayerBank(int p)
{
	for (int i = 1; i <= p+1; i++)
		bank[i] = 1000;
}

//--- This function initialize player's playing
void Blackjack::setPlayer(int p)
{
	for (int j = 1; j <= p+1; j++)
		players[j] = j; 
}

//--- This function initialize player's initial card values
void Blackjack::setCardValues(int p)
{
	for (int k = 1; k <= p+1; k++)
		cardValues[k] = 0;
}

//--- This function play a round of Blackjack
void Blackjack::playRound()
{
	cout << endl << endl;;
	
	while (gameRound_ <= gameLimit_)
	{
		int betAmount = 0;
		int i = 1;
		
		cout << " =+=+=+=+=+=+= Round [" << gameRound_ << "] =+=+=+=+=+=+= " << endl;
		setCardValues(numPlayers_);

		while (i <= numPlayers_ )
		{
			cout << "Player #" << players[i] << endl;
			cout << "You have $" << bank[i] << "." << endl;
			cout << "How much do you want to bet? " ;
			
			// loop until user enters a valid input
			while (!(cin >> betAmount) || betAmount < 5 || betAmount > 1000)
			{
				cout << "How much do you want to bet? " ;
				cin.clear();
				cin.ignore(100, '\n');
			}

			playersBet [i] = betAmount;
			i++;
			cout << endl;
		}

		cout << "	********** Dealing 2 cards per player ********** " << endl;
	
		getDealerCards();
		giveCards();

		for (int j = 1; j <= numPlayers_; j++)
		{
			cout << "--> Player #"<< players[j] << endl;				
			takeTurns(j);
			cout << endl;
		}

		playDealer();
		settleBets();
		cout << endl;
		gameRound_++;
	}

	gameOver();
	
}

//--- This function gets dealers cards
void Blackjack::getDealerCards()
{
	cout << "	Dealer :	";
	
	// Dealer is the last player, the cardValues at the last index holds dealers' card
	int dealerIndex = numPlayers_+1;
	
	Card dealer;
	
	dealer = playingDeck.getCard();		// get the first card
	cardValues[dealerIndex] += dealer.getRank();	
	cardHolder[1][dealerIndex] = dealer;
	dealer.displayHole();				// do not show dealer's first card
	cout << "	";
	
	dealer = playingDeck.getCard();		// get the second card and add to the value
	cardValues[dealerIndex] += dealer.getRank();
	cardHolder[2][dealerIndex] = dealer;
	dealer.display();				
	cout << endl;

	if ( (cardValues[dealerIndex] == MAX) )
	{
		cout << "Cards	: ";
		displayStatus(dealerIndex, 2);
		cout << endl << "Blackjack!! " << endl << endl;
		blackjack(dealerIndex);
		return;
	}

}

//--- This function give each player two cards
void Blackjack::giveCards()
{
	for ( int i = 1; i <= numPlayers_; i++ )
	{
		cout << "	Player #" << players[i] << " :	";
		for ( int j = 1; j < 3; j++ )
		{
			Card draw = playingDeck.getCard();
			cardValues[i] += draw.getRank();
			cardHolder[j][i] = draw;
			draw.display();
			cout << "	";
		}
		cout << " ; Total is " << cardValues[i] << endl;
	}
	cout << endl << endl;
}

//--- This function displays the current status/result when playing
void Blackjack::displayStatus(int playerNum, int next)
{
	for (int a = 1; a <= next; a++)
	{
		cardHolder[a][playerNum].display();
		cout << " ";
	}
	cout << "		; Total is " << cardValues[playerNum];
}

//--- This function give each player a turn to play
void Blackjack::takeTurns(int playerNum)
{
	int numOfCards = 2;
	char d; 
	
	cout << "Cards	: ";
	displayStatus(playerNum, numOfCards);
	cout << endl << "Hit (h) or Stand (s)? ";
	
	// loop until user enters the correct input
	while (!(cin >> d) ||( d != 'H' && d != 'S' && d != 'h' && d != 's') )
	{
		cout << " Hit (h) or Stand (s)? " ;
		cin.clear();
		cin.ignore(100, '\n');
	}
	
	// continue to 'Hit' until player decided 'Stand'
	while ( d != 's' ) 
	{
		numOfCards++;
		Card addToHand;
		addToHand = playingDeck.getCard();
		cardValues[playerNum] += addToHand.getRank();
		cardHolder[numOfCards][playerNum] = addToHand;
		
		if ( (cardValues[playerNum]) > MAX )
		{
			cout << "Cards	: ";
			displayStatus(playerNum, numOfCards);
			cout << endl << "BUST!" << endl;
			return;
		}

		if ( (cardValues[playerNum] == MAX) )
		{
			cout << "Cards	: ";
			displayStatus(playerNum, numOfCards);
			cout << endl << "Blackjack!! " << endl << endl;
			blackjack(playerNum);
			return;
		}
		
		cout << "Cards	: ";
		displayStatus(playerNum, numOfCards);
		cout << endl << "Hit (h) or Stand (s)? " ;
		
		while (!(cin >> d) ||( d != 'H' && d != 'S' && d != 'h' && d != 's'))
		{
			cout << " Hit (h) or Stand (s)? " ;
			cin.clear();
			cin.ignore(100, '\n');
		}
	}
	return;
}

//--- This function ends the current game if a 21 is found, and play the next round
void Blackjack::blackjack(int playerNum)
{
	settleBets();
	gameRound_++;
	playRound();
}

//--- This function play the dealer
void Blackjack::playDealer()
{
	int nextCard = 2;
	int exit = 0;
	int dealerIndex = numPlayers_+1;

	// dealer play 'Hit' on 16 and 'Stand' on 17
	while (cardValues[dealerIndex] < 17) 
	{
		nextCard++;
		Card addToHand;
		addToHand = playingDeck.getCard();
		cout << "DEALER draws	";
		addToHand.display();
		cout << endl;
		cardValues[dealerIndex] += addToHand.getRank();
		cardHolder[nextCard][dealerIndex] = addToHand;
	} 
	cout << "Dealer's hand:	";
	int index = 1;
	while(index <= nextCard)
	{
		cardHolder[index][dealerIndex ].display();
		cout << " ";
		index++;
	}
	cout << "	; Total is " << cardValues[dealerIndex];
	cout << endl << endl;
	return;
	
}

//--- This function settle the bets
void Blackjack::settleBets()
{
	// remove busted cards by setting them to zero 
	for (int s = 1; s <= numPlayers_+1; s++)
	{
		if (cardValues[s] > 21)
			cardValues[s] = 0;
	}

	// dealer holds the largest card values
	int largestValue = cardValues[numPlayers_+1]; 
	
	// nobody is the initial winner
	int winner = 0; 
	
	// find the largest card values and compare it to the dealer's card
	int i = 1;
	while (cardValues[i] <= 21 && i <= (numPlayers_+1))
	{
		if (cardValues[i] > largestValue )
		{
			largestValue = cardValues[i];
			winner = i;
		}
		i++;
	}

	// updates each player's bank and display the results
	for (int b = 1; b <= numPlayers_; b++) 
	{
		cout << "Player #" << players[b];
		if (b == winner)
		{
			cout << " wins " << playersBet[winner] << endl;
			bank[winner] += playersBet[winner];
		}
		else if (cardValues[b] == 1)
		{
			cout << " pushes " << playersBet[b] << endl;
			bank[b] = bank[b];
		}
		else if (cardValues[b] == 0)
		{
			cout << " loses " << playersBet[b] << endl;
			bank[b] -= playersBet[b];
		}
		else if(cardValues[b] == cardValues[numPlayers_+1])  // pushes --> if a player's card value is the same as the dealer's card value
		{
			cout << " pushes " << playersBet[b] << endl;
			bank[b] = bank[b];
		}
		else
		{
			cout << " loses " << playersBet[b]<< endl;
			bank[b] -= playersBet[b];
		}
		
	}
}

//--- This function ends the game, displays the result and asks if user want to play a new round
void Blackjack::gameOver()
{
	char input;
	cout << "+=+=+=+=+=+= Thanks for playing +=+=+=+=+=+=" << endl;
	for (int j=1; j <= numPlayers_; j++)
	{
		cout << "Player #" << players[j] <<
			" has " << bank[j] << " in the bank." << endl;
	}
	
	cout << endl << "Do you want to play another game? Yes (Y) or No (N) ";
	while(!(cin>>input) ||( input != 'Y' && input != 'y' && input != 'N' && input != 'n'))
	{
		cout << endl << "Do you want to play another game? Yes (Y) or No (N) ";
	}
	if ((input == 'Y') || (input == 'y'))
	{
		newGame();
	}
	else
	{
		system ("PAUSE");
		exit(0);
	}
}

//--- This function displays the initial status of player's bank
void Blackjack::displayInitial()
{
	cout << endl << "The players:";
	for (int j = 1; j <= (numPlayers_); j++)
	{
		cout << "Player #" << players[j] << 
			" has $" << bank[j] << " in the bank."<< endl;
	}
	cout << "This game will be played for " << gameLimit_ << " rounds." << endl;
	
}
