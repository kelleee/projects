/*	File	: Deck.cpp
 *	Modified: October 28, 2013
 *
 * ----- This file defines the Deck class
 */

#include "Deck.hpp" 
#include "Card.hpp"
#include <iostream>
#include <string>

Deck::Deck()
{
	int index = 0;
	for (int s = 3; s <= 6; s++) // from ASCII table, char heart, diamonnds, club and spade is value at 3 to 6 is the shape
	{
		for (int r = 1; r <= 13; r++)
		{
			hand[index] = Card(r, char (s));
			index++;
		}
	}
}


//--- This function gets/draws a card from the deck
Card Deck::getCard()
{
	this->draw_ = hand[rand()%52];
	return draw_; 
}

//--- This function shuffle the cards
void Deck::shuffle()
{		
	for (int i=0; i <52; i++)
	{
		hand[i].swap(hand[rand()%52]);
	}
}

//--- This function displays all the cards
void Deck::displayCards()
{
	cout << endl;

	for (int i = 0; i < 52; i++)
	{
		if (i % 13 == 0)
		{
			cout << endl;
		}
		hand[i].display();
		cout << '\t' ;
	}
	cout << endl;
}




