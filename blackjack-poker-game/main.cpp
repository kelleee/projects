/*	File	: main.cpp
 *	Modified: October 28, 2013
 *
 * ----- This file source file for Blackjack game
 */

#include "Card.hpp"
#include "Deck.hpp"
#include "Blackjack.hpp"
#include <iostream>
#include <Windows.h>
#include <string>

void instructions();

int main()
{
	srand((unsigned)(time(0)));

/*	// @Test ---> Card
	// construct a default card with rank = 0, suit = ' '
	Card testCard;				
	testCard.display();
	cout << endl << "This rank: " << testCard.getRank() << endl
				 << "This suit: " << testCard.getSuit() << endl;
	
	// display dealers' hole card
	testCard.displayHole();
	cout << endl;
	
	// set a card to specific value
	testCard.setCard(3,3);		
	testCard.display();
	cout << endl << "This rank: " << testCard.getRank() << endl
				 << "This suit: " << testCard.getSuit() << endl;

	
	// @Test ---> Deck
	// construct a default deck of card in ordered rank and suit
	Deck testDeck;
	testDeck.displayCards();

	// shuffle the card
	
	testDeck.shuffle();
	testDeck.displayCards();
	
	// get 10 cards from the shuffled deck
	cout << "Ten random cards from the deck" << endl;
	for (int i=0; i < 10; i++)
	{
		testDeck.getCard().display();
		cout << "	";
	}
	cout << endl;
*/



/*	//@ Test ---> Blackjack
	Blackjack testGame(3, 5);		// creates a game with 3 players and play 10 rounds
	testGame.displayInitial();
	testGame.playRound();
*/	

	
	// play Blackjack

	Blackjack game;

	cout <<"+++++++++++++++++++++++++++++ Welcome to Blackjack ++++++++++++++++++++++++++++" << endl << endl;
	instructions(); // displays game instructions and rules
	game.newGame(); // play the game 

	system("PAUSE");
}


// Function to display the instruction for the game
void instructions()
{
	cout << "\n -> This game can be played only up to 10 players and is limited to 10 rounds\n";
	cout << " -> Each player receives an initial money in bank, $1000\n";
	cout << " -> Basic rules:\n	1) The cards are valued as follows:\n		- An Ace is count as 1\n		- The 10, Jack, Queen and King are all valued at 10" 
		 << "\n	2) A blackjack is a total of 21 in your cards." 
		 << "\n	3) Minimum bet is $5."
		 << "\n	4) Each hand will result in one of the following events for the player:"
		 << "\n		- Lose	: The player's bet is taken by the dealer."
		 << "\n		- Win	: The player wins as much as he bet. "
		 << "\n		- Push	: The hand is a draw. The player keeps his bet, "
		 << "\n		neither winning nor losing money.";

	cout << "\n\nPress CTRL+C to exit the game";
	cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n";
}

// Function to handle the CTRL-C Signal
BOOL CtrlHandler( DWORD fdwCtrlType ) 
{ 
  switch( fdwCtrlType ) 
  { 
    // Handle the CTRL-C signal. 
    case CTRL_C_EVENT: 
      printf( "Ctrl-C event\n\n" );
      Beep( 750, 300 ); 
      return( TRUE );
	  default: 
      return FALSE; 
  }
}
