/*	File	: Blackjack.hpp
 *	Modified: October 28, 2013
 *
 * ----- This file declares the Blackjack class
 */

#ifndef _BLACKJACK_HPP_
#define _BLACKJACK_HPP_

#include "Card.hpp"
#include "Deck.hpp"

class Blackjack {
	public:

		Blackjack ();

		/** Constructors
		  * This function construct a blackjack game based on number of players and game limit
		  * @param numPlayers	--> number of players playing
		  * @param gameLimit	--> number of turns to be playes
		  */
		Blackjack (int numPlayers, int gameLimit); 

		// Function to create a new game if user decided to play again
		void newGame();

		// Function to place a default amount of money for each player
		void setPlayerBank (int numPlayers);

		// Function to set the player in an array
		void setPlayer (int numPlayers);
	
		// Funtion to display the player initial card values in hand
		void setCardValues(int);

		// Function to play a round of Blackjack
		void playRound ();

		// Function to get cards for the dealer
		void getDealerCards ();
	
		// Function to give cards to each player
		void giveCards ();

		// Function to display cards in hand while playing
		// @param playerNum	--> the index of each player
		// @param next		--> the total number of cards drawn while playing
		void displayStatus(int playerNum, int next);

		// Function for each player to take turns to play
		// @param playerNum	--> the index of each player 
		void takeTurns(int playerNum);

		// Functions to settle the bet and play a new round when a player gets blackjack
		void blackjack(int playerNum);

		// Function for dealer to play
		void playDealer();
	
		// Function to find a winner and settle bets
		void settleBets();
	
		// Function to display the amount of money left 
		void gameOver();

		// Function to display the number of players and their initial money in bank
		void displayInitial ();

		// Function to display instructions and interface
		void intructions ();
		

	private:

		// private variable declarations
		int numPlayers_;
		int gameLimit_;
		int gameRound_;
		
		// These arrays are set to a size of 11 (10 players max in each game)
		// 11th player is the dealer
		int players[11];		 // to hold all players
		Card cardHolder[10][11]; // to hold the card for each player, set maximum to 10 cards per player
		int cardValues[11];		 // to add the cards value for each player
		int bank[11];			 // to hold player's money bank
		int playersBet[11];		 // to hold player's bet amount

};

#endif