A program that plays blackjack using the **object-oriented programming** technique.

**Note** Each individual cpp files should be compiled in **Visual Studio 2012**. 

**Running the application**
You have to make sure that your **.exe** runs in a Windows environment with Visual Studio 2012 installed.

**Program overview**

1. Create and shuffle the deck.
2. Ask how players there are and how many turns will be played 
3. Put $1000 in each player's “bank”. 
4. For each round you're going to play:
	1. Ask each player for their bet.
	2. Give two cards to each player and the dealer (display all but the dealer's hole card).
	3. Let each player take their turn:
		1. Ask the player whether they want to hit or stand, until they either stand or bust. 
		2. Each card the player is given should be added to their hand,and their new total calculated appropriately.
	4. Play the dealer's hand. (Hit on 16, stand on 17.) 
	5. Settle the bets: give/take the bet for each player.
5. Game over. Display the amount of money in each player's bank.