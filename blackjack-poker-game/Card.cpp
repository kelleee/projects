/*	File	: Card.cp
 *	Modified: October 28, 2013
 *
 * ----- This file defines the Card class
 */

#include "Card.hpp"
#include <iostream>

using namespace std;

Card::Card(int rank, char suit)
{
	this->rank_ = rank;
	this->suit_ = suit;
}

//--- This function set the rank and suit of the card
void Card::setCard(int rank, char suit)
{
	rank_ = rank;

	suit_ = suit;
}

//--- This function swaps the cards
void Card::swap(Card& c)
{
	Card temp;

	temp.rank_ = c.rank_;
	temp.suit_ = c.suit_;

	c.rank_ = this->rank_;
	c.suit_ = this->suit_;
}

//--- This function displays all the cards
void Card::display()
{
	// switch the rank_ number of Jack, Queen, King and ACE
	// to display their respective character instead of values
	switch (rank_)
	{
		case 11: cout << 'J';
			break;
		case 12: cout << 'Q';
			break;
		case 13: cout << 'K';
			break;
		case 1: cout << 'A';
			break;
		default: cout << rank_ ;
	}
	
	cout << suit_;
}

//--- This function display dealer's hole card
void Card::displayHole()
{
	switch (rank_)
	{
		case 1: cout << "*";
			break;
		default: cout << "*";
	}
	switch (suit_)
	{
		case 1: cout << "*";
			break;
		default: cout << "*";
	}
}

//--- Getter function for the rank
int Card::getRank()
{
	int rank;
	if (rank_ == 11 || rank_ == 12  || rank_ == 13 )
		rank = 10;
	else
		rank = rank_;
	return rank;
}

//--- Getter function for the suit
char Card::getSuit()
{
	return suit_;
}

