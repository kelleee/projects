import static org.junit.Assert.*;

import org.junit.Test;

public class StackMP2Test {

	/*
	 * This test creates a new stack and pushes a tag to it.
	 * 
	 * test validates constructor method as well as push() and isEmpty() method
	 */
	@Test
	public void test() {
		StackMP2 test = new StackMP2();
		HtmlTag tag = new HtmlTag("thisTag");
		test.push(tag);
		assertEquals(false, test.isEmpty());
	}
	
	/*
	 * This test pushes a new tag to the stack and peeks at it.
	 * 
	 * test2 is used to try the peek() method
	 */
	@Test
	public void test2() {
		StackMP2 test = new StackMP2();
		HtmlTag tag = new HtmlTag("thisTag");
		test.push(tag);
		assertEquals(tag, test.peek());
	}
	
	/*
	 * This test pushes two tags to the stack, and pops the top
	 * 
	 * test3 validates the pop() method will take the top of the stack
	 */
	@Test
	public void test3() {
		StackMP2 test = new StackMP2();
		HtmlTag tag = new HtmlTag("thisTag");
		HtmlTag tag2 = new HtmlTag("tag2");
		test.push(tag);
		test.push(tag2);
		assertEquals(tag2, test.pop());
	}
	
}
