import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

public class HtmlValidatorTest {
	/*
	 * test tests valid queue
	 * validates constructos, getTags() 
	 */
	@Test
	public void test() {
		// sample tags: <html><body><b>hello</b><i>how are <b>you</b><br/></i></body></html>
		Queue <HtmlTag> myQueue = new LinkedList<HtmlTag>();
		myQueue .add(new HtmlTag("html", true));   // <html>
		myQueue .add(new HtmlTag("body", true));   // <body>
		myQueue .add(new HtmlTag("b", true));      // <b>
		myQueue .add(new HtmlTag("b", false));     // </b>
		myQueue .add(new HtmlTag("i", true));      // <i>
		myQueue .add(new HtmlTag("b", true));      // <b>
		myQueue .add(new HtmlTag("b", false));     // </b>
		myQueue .add(new HtmlTag("br"));           // <br/>
		myQueue .add(new HtmlTag("i", false));     // </i>
		myQueue .add(new HtmlTag("body", false));  // </body>
		myQueue .add(new HtmlTag("html", false));  // </html>
  
        HtmlValidator validator = new HtmlValidator(myQueue);
        assertEquals("getTags", myQueue , validator.getTags());

	}
	
	/*
	 * test2 tests invalid data
	 */
	@Test
	public void test2() {
		// Test for invalid code and setTags
		HtmlValidator validator = new HtmlValidator();
		Queue<HtmlTag> myQueue = new LinkedList<HtmlTag>();
		myQueue.add(new HtmlTag("html", true));   // <html>
		myQueue.add(new HtmlTag("body", true));   // <body>
		myQueue.add(new HtmlTag("b", true));      // <b>
		myQueue.add(new HtmlTag("body", false));  // </body>
		myQueue.add(new HtmlTag("b", false));     // </b>
		myQueue.add(new HtmlTag("html", false));  // </html>
	       
		for (HtmlTag tag : myQueue) {
			validator.addTag(tag);
		}
		assertEquals("getTags", myQueue, validator.getTags());
		}
}
