/* 
 * This is the HtmlValidator class.
 * 
 * @Authors: Kelly Anyi, Stephanie Lam, Theresa Mammarella
 */

import java.util.LinkedList;
import java.util.Queue;

public class HtmlValidator {
	
	public int lengthQ;
	int firstInQ;
	private Queue <HtmlTag> html_queue;	
	
	/*
	 * Modifies: initializes firstInQ
	 * Effects: Creates an empty HTML queue
	 * 
	 * @param html_queue- array to be initialized
	 */
	public HtmlValidator(){
		html_queue = new LinkedList<HtmlTag>();
		firstInQ = -1;
	}
	
	/*
	 * Requires: HTML queue
	 * Effects: initializes a second, identical queue
	 * 
	 * @param Queue of HTML tags
	 * @throws IllegalArgumentException if queue is empty
	 */
	public HtmlValidator(Queue<HtmlTag> tags){
	
		if (tags == null)
			throw new IllegalArgumentException ("Invalid input! Cannot pass null!");
		else
			html_queue = tags;
		
	}
	
	/*
	 * Requires: an HTML tag
	 * Modifies: inserts tag into queue
	 * Effects: adds a new tag to the end of the queue
	 * 
	 * @param HtmlTag
	 * @return void
	 * @throws IllegalArgumenntException if queue is empty
	 */
	public void addTag(HtmlTag tag){
		
		if (tag == null)
			throw new IllegalArgumentException ("Invalid input! Cannot pass null!");
		else
			html_queue.add(tag);
	}
	
	/*
	 * Effects: Returns the queue of HTML tags
	 * 
	 * @return html_queue
	 */
	public Queue<HtmlTag> getTags(){
		
		return html_queue;
	}
	
	/*
	 * Requires: specific HTML element
	 * Modifies: html_queue
	 * Effects: removes any tags that match the passed element
	 * 
	 * @param String element
	 * @param lengthQ - size of the queue
	 * @param find - the element at the head of the queue
	 * @return void
	 */
	public void removeAll(String element){
		
		int lengthQ = html_queue.size();
		String find;
		
		for ( int i=0; i < lengthQ; i++ )
		{
			find = html_queue.peek().getElement();
			
			if( find.equals(element) )
			{	
				html_queue.remove();
				i--;
			}
			else
			{
				html_queue.add( html_queue.peek() );
				html_queue.remove();
			}
		}
	}
	
	/*
	 * Requires: HTML queue
	 * Effects: prints indented text representation of HTML tags in queue,
	 * displays warning when tags are not valid
	 * 
	 * @return void
	 */
	public void validate(){
		
		StackMP2 tempStack = new StackMP2();
		int indent = 0;
		
	     for(int i = 0; i < html_queue.size(); i++) { 
	    	 if(html_queue.peek().isOpenTag()) {
	    		for(int j = 0; j < indent; j++) {
	    			 System.out.print("\t");
	    		}
	    		 
	    		if(html_queue.peek().isSelfClosing()) {		
	    			System.out.println(html_queue.remove());
	    			i--;
	            } else {
	            	indent++;
	            	System.out.println(html_queue.peek());
	            	tempStack.push(html_queue.remove());
	            	i--;
	            }    
	    	} else if(!html_queue.peek().isOpenTag()) {
	    		if(tempStack.isEmpty()) {
	    			System.out.println("ERROR unexpected tag:" + html_queue.remove()); 
	    		} else if(html_queue.peek().matches(tempStack.peek()) && !tempStack.isEmpty()){
	    			indent--;
	    		
	    			for(int j = 0; j < indent; j++) {
	    				System.out.print("\t");
	    			}
	    		
	    			System.out.println(html_queue.peek());
	    			tempStack.pop();
	    			html_queue.remove();
	    			i--;
	    		} else {
	    			System.out.println("ERROR unexpected tag:" + html_queue.peek());
	    			html_queue.remove();
	    			i--;
	    		}
	    	}
	     }
	     
	     while(!tempStack.isEmpty()){
	    	 System.out.println("ERROR unclosed tag:" + tempStack.pop());
	    }
	}

}