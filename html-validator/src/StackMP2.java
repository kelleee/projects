/*
 *Note:this file was completed in class as a group.
 *Please assign equal credit despite GitHub indication.
 *
 *@Authors: Theresa Mammarella, Kelly Anyi, Stephanie Lam
 *
 * Implementation of a simple stack for HtmlTags.
 */

import java.util.ArrayList;

public class StackMP2 {
	
	private int topStack;
	private ArrayList<HtmlTag> stack_internal;
	
	/*
	 * Modifies: initializes topStack
	 * Effects: Creates an empty stack
	 * 
	 */
	public StackMP2( ) {
		this.stack_internal = new ArrayList<HtmlTag>( );
		topStack=-1;
	}
	
	/*
	 * Requires: passed HTML tag
	 * Modifies: Stack by pushing HTML tag to the top
	 * Effects: Pushes HtML tag onto stack
	 * 
	 * @param HTML tag
	 * @return void
	 */
	public void push( HtmlTag tag ) {
		topStack=topStack+1;
		stack_internal.add(topStack,tag);	
	}
	
	/*
	 * Requires: a non empty stack
	 * Modifies: removes and returns top HTML tag
	 * Effects: Pops HTML tag from stack
	 * 
	 * @return HTML tag
	 * @throws NullPointerException when stack is empty
	 */
	public HtmlTag pop( ) {
		
		if ( stack_internal.isEmpty() ) {
			throw new NullPointerException( "Stack is empty!");
		}
		
		HtmlTag tag = stack_internal.get(topStack);
		stack_internal.remove(topStack);
		topStack=topStack-1;
		return tag;
	}
	
	/*
	 * Requires: a non empty stack
	 * Effects: Method looks at top tag and returns it
	 * without removal from stack
	 * 
	 * @return HTML tag
	 * @throws NullPointerException when stack is empty
	 */
	public HtmlTag peek( ) {
		
		if ( stack_internal.isEmpty() ) {
			throw new NullPointerException ("Stack is empty!");
		}
		
		HtmlTag tag = stack_internal.get(topStack);
		return tag;
	}
	
	/*
	 * Effects: tests is the stack is empty
	 * 
	 * @return true if stack is empty, false otherwise
	 */
	public boolean isEmpty( ) {
		if (topStack == -1  )
			return true;
		else
			return false;
	}
}
