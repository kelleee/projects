## Projects ##

Mini projects throughout university. 

### Blackjack Poker Game ###
A computer game built with MS Visual Studio. It uses C++ programming language and based on the object-oriented programming (OOP).

### Client Server Program ###
A simple client-server program that uses TCP. It uses C programming language.

### HTML Validator ###
A program that validates a HTML page. It uses Java programming language.

### Music Catalogue ###
A program that organizes songs, albums and artists. It uses Java programming language.