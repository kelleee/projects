Simple TCP Server-Client program. 
The client sends one simple string to the server, and receives back a response (another string) from the server. 

**Note** Each individual c files should be compiled in **Visual Studio**. 

**Running the application:**
The server program should be started before the client program.

To execute the server, compile the server source code (server.c) and run the executable file. The server application listens on TCP port 80 for a client to connect. Once a client connects, the server receives data from the client and prompts user to type a message (the data) to be received back by the client. When the client shuts down the connection, the server shuts down the client socket, closes the socket, and exits.

To execute the client, compile the client source code and run the executable file. The client application requires that name of the computer or IP address of the computer where the server application is running is passed as a command-line parameter when the client is executed. If the client and server are executed on the sample computer, the client can be started as follows:

**client localhost**

The client tries to connect to the server on TCP port 80. Once the client connects, the client sends data to the server and receives any data send back from the server. The client then closes the socket and exits. 