/*
 * Simple TCP Server program
	1. Initialize Winsock.
	2. Create a socket.
	3. Bind the socket.
	4. Listen on the socket for a client.
	5. Accept a connection from a client.
	6. Receive and send data.
	7. Disconnect
 */

#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h> // contains definitions that includes newer functions and structures used to retrieve IP addresses. 

#pragma comment(lib, "Ws2_32.lib")	// link the build environment to Winsock Library file "Ws_32.lib"

#define DEFAULT_PORT 80
#define BUF_SIZE 1024
#define BACKLOG 10
#define DEBUG 0

int __cdecl main(void){
	WSADATA wsa;
	SOCKET s, new_s;
	struct sockaddr_in server, client;
	char recv_buf[BUF_SIZE];
	char send_buf[BUF_SIZE];
	int client_len = sizeof(client);
	int recv_byte;
	int send_byte;

	/* Initialize Winsock */
	if (DEBUG) printf("Initializing Winsock...\n");

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0){ // WSAStartup function initialize the Winsock library. Use Winsock version 2
		printf("Failed. Error code: %d", WSAGetLastError());
		return 1;
	}

	printf("TCP Server started...\n");

	/* Create a socket */
	s = socket(AF_INET, SOCK_STREAM, 0); // creates a socket and returns the socket descriptor
	if (s == INVALID_SOCKET){
		printf("Unable to create socket: %d", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	if (DEBUG) printf("Socket created.\n");

	memset((char *)&server, 0, sizeof(struct sockaddr_in));
	server.sin_family = AF_INET; 
	server.sin_port = htons(DEFAULT_PORT);
	server.sin_addr.s_addr = INADDR_ANY; 

	/* Bind the socket */
	if (bind(s, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR){
		printf("Bind failed. Error code: %d", WSAGetLastError());
		closesocket(s);
		WSACleanup();
		return 1;
	}
	if (DEBUG) printf("Bind succeeded.\n");

	/* Listen to an incoming connection */
	listen(s, BACKLOG);

	printf("TCP Server is waiting for client on PORT %d\n", DEFAULT_PORT);
	printf("Enter 'q' to quit the program.\n");

	while(1){

		/* Accept an incoming connection */
		if ((new_s = accept(s, (struct sockaddr *)&client, &client_len)) == INVALID_SOCKET){
			printf("accept failed. Error code: %d", WSAGetLastError());
			return 1;
		}
		printf("Accepted connection from: %s\n", inet_ntoa(client.sin_addr));
		
		while (1){

			/*  Receive data from client */
			memset(recv_buf, sizeof(recv_buf), 0);
			if ((recv_byte = recv(new_s, recv_buf, sizeof(recv_buf), 0)) < 0){
				printf("accept failed. Error code: %d", WSAGetLastError());
				return 1;
			}
			else if (recv_byte == 0){
				printf("Connection with %s ended\n\n", inet_ntoa(client.sin_addr));
				printf("TCP Server is waiting for client on PORT %d\n", DEFAULT_PORT);
				break;
			}
			else{
				recv_buf[recv_byte] = '\0'; // Terminate the string
				printf("\n > Message received: %s", recv_buf);
				if (DEBUG) printf("Bytes received: %d\n", recv_byte);
			}

			/*  Send data to client */
			memset(send_buf, 0, sizeof(send_buf));
			printf(" > Write to client: ");		// prompt user to enter message to send to client
			fgets(send_buf, BUF_SIZE, stdin);	// read input from command line and fill send_buf
			if (send_buf[0] == 'q' && strlen(send_buf) == 2){
				printf("\nQuitting program...\n");
				printf("Closing connection...\n");
				closesocket(new_s);
				closesocket(s);
				WSACleanup();
				printf("\nTCP Server ended\n");
				exit(1);
			}

			if ((send_byte = send(new_s, send_buf, strlen(send_buf), 0)) < 0){
				printf("send failed. Error code: %d", WSAGetLastError());
				return 1;
			}
			else if (send_byte == 0){
				printf("Ending connection... \n");
			}
			else{
				if (DEBUG) {
					printf("Message sent: %s\n", send_buf);
					printf("Bytes sent: %d\n", send_byte);
				}
			}
		}
		closesocket(new_s);	
	};

	printf("Closing connection...\n");
	
	/* Cleanup */
	closesocket(s);
	WSACleanup();

	return 0;
}