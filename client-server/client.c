/*
 * Simple TCP Client program
	1. Initialize Winsock.
	2. Create a socket.
	3. Connect to the server.
	4. Send and receive data.
	5. Disconnect.
*/

#include <stdio.h>
#include <string.h>
#include <winsock2.h>
#include <ws2tcpip.h> // contains definitions that includes newer functions and structures used to retrieve IP addresses. 

#pragma comment(lib, "Ws2_32.lib")	// link the build environment to Winsock Library file "Ws_32.lib"

#define DEFAULT_PORT 80
#define BUF_SIZE 1024
#define DEBUG 0

int __cdecl main(int argc, char *argv[]){
	WSADATA wsa;
	SOCKET s;
	struct sockaddr_in server;
	struct hostent *sp;
	char *hostname = NULL;
	char send_buf[BUF_SIZE];
	char recv_buf[BUF_SIZE];
	int recv_byte;
	int send_byte;

	if (argc != 2){
		printf("usage: client <hostname>\n");
		return 0;
	}

	hostname = argv[1];

	/* Initialize Winsock */
	if (DEBUG) printf("Initializing Winsock...\n");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0){ // WSAStartup function initialize the Winsock library. Use Winsock version 2
		printf("Failed. Error code: %d", WSAGetLastError());
		return 1;
	}

	printf("TCP Client started...\n");

	/* Create a socket */
	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == SOCKET_ERROR){
		printf("Unable to create socket: %d", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	if (DEBUG) printf("Socket created.\n");
	
	/* Getting HOST information */
	memset((char *)&server, 0, sizeof(struct sockaddr_in));
	if (inet_addr(hostname) == INADDR_NONE){
		sp = gethostbyname(hostname);
	}
	else {
		server.sin_addr.s_addr = inet_addr(hostname);
		sp = gethostbyaddr((char *)&server.sin_addr.s_addr, sizeof(server.sin_addr.s_addr), AF_INET);
	}

	if (sp == NULL){
		printf("Invalid hostname '%s'\nConnection closing...\n", hostname);
		closesocket(s);
		WSACleanup();
		return 1;
	}

	/* Connect to the server */
	server.sin_family = AF_INET;
	server.sin_port = htons(DEFAULT_PORT);
	server.sin_addr.s_addr = *((unsigned long *)sp->h_addr);

	if ((connect(s, (struct sockaddr *) &server, sizeof(struct sockaddr_in))) == SOCKET_ERROR){
		printf("Unable to connect to server!\n");
		closesocket(s);
		WSACleanup();
		return 1;
	}
	
	printf("TCP Client is connected to server: %s @ IP: %s \n", hostname, inet_ntoa(server.sin_addr));
	printf("Enter 'q' to quit the program.\n");

	do{
		
		/* Send data to the server */
		memset(send_buf, 0, sizeof(send_buf));
		printf("\n > Write to server: ");		// prompt user to enter message to send to client
		fgets(send_buf, BUF_SIZE, stdin);	// read input from command line and fill send_buf
		if (send_buf[0] == 'q' && strlen(send_buf) == 2){
			printf("\nQuitting program...\n");
			break;
		}

		if ((send_byte = send(s, send_buf, strlen(send_buf), 0)) < 0){
			printf("send failed. Error code: %d", WSAGetLastError());
			return 1;
		}
		else if (send_byte == 0){
			printf("Ending connection... \n");
		}
		else{
			if (DEBUG) {
				printf("Message sent: %s\n", send_buf);
				printf("Bytes sent: %d\n", send_byte);
			}
			
			/* Receive data from the server */
			memset(recv_buf, sizeof(recv_buf), 0);
			if ((recv_byte = recv(s, recv_buf, sizeof(recv_buf), 0)) < 0){
				printf("accept failed. Error code: %d", WSAGetLastError());
				return 1;
			}
			else if (recv_byte == 0){
				printf("\nConnection with server %s ended!\n", inet_ntoa(server.sin_addr));
				printf("Ending connection... \n");
				break;
			}
			else{
				recv_buf[recv_byte] = '\0'; // Terminate the string with a NULL character
				printf(" > Message received: %s", recv_buf);
				if (DEBUG) printf("Bytes received: %d\n", recv_byte);
			}
		}
	} while (1);

	/* Shutdown connection */
	if (shutdown(s, SD_SEND) == SOCKET_ERROR){
		printf("shutdown failed. Error code: %d\n", WSAGetLastError());
		closesocket(s);
		WSACleanup();
		return 1;
	}

	printf("Closing connection...\n");
	
	/* Cleanup */
	closesocket(s);
	WSACleanup();
	
	printf("\nTCP Client ended \n\n");
	
	return 0;
}